Attribute VB_Name = "Constants"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
'
'            CONSTANTS
'

' Version of abk_lib
Public Const REVISION = "1.1 - 22-Jan-2017"

' Used in VerticalTable.findRowInTable if no matching row was found.
Public Const NO_VALUE = -1

'The maximum number of columns supported by this macro library
Public Const MAX_COLUMNS = 16384

'The maximum number of rows supported by this macro library
Public Const MAX_ROWS = 1048576

' Standard delimiter to separate values in a string. Used in function StringOps.findStringInCsvList
Public Const STD_FIELD_DELIMITER = ","


