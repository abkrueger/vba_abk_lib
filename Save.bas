Attribute VB_Name = "Save"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

' This is a work around - in case Excel doesn't save the xla file via the designated UI save button.
' This has happened with older Excel installations.
'
' !!!
'     NOTE: Excel does not produce any warnings
'     when the workbook is closed without saving the xla through any of the UI menus.
' !!!
'

' The sub below will enforce the save when you run it.
Public Sub save()
    Workbooks("abk_lib.xlam").save
End Sub

'Exports all macros of abk_lib to .bas, .cls, .frm and .frx files in directory ...Documents/VBA
Public Sub exportAll()
    Dim Names As New Collection
    Call Names.Add("Constants")
    Call Names.Add("Constructors")
    Call Names.Add("Logger")
    Call Names.Add("Logging")
    Call Names.Add("MacroOutput")
    Call Names.Add("PanicWindow")
    Call Names.Add("save")
    Call Names.Add("StringOps")
    Call Names.Add("Utils")
    Call Names.Add("VerticalTable")

    Dim n
    For Each n In Names
        Dim extStr As String
        Dim typ
        typ = Workbooks("abk_lib.xlam").VBProject.VBComponents(n).Type
        If typ = 1 Then
            extStr = ".bas"
        ElseIf typ = 2 Then
            extStr = ".cls"
        ElseIf typ = 3 Then
            extStr = ".frm"
        Else
            extStr = ""
        End If
        Call Workbooks("abk_lib.xlam").VBProject.VBComponents(n).Export("VBA\" & n & extStr)
    Next n
End Sub
