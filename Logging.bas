Attribute VB_Name = "Logging"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
' Excel VBA Module: Logging
'
' DESCRIPTION:
'
' This module provides functions to log and track the progress of Excel macros.
' The front end is implemented in the form "MacroOutput".
' The API that shall be used by the Macro programmer is defined by the class <Logger>.
'
' This module is the interface between the front end and the API.
'

Option Explicit

'***************************************************************
'
'            CONSTANTS
'

' These are the supported log levels. The can help the programmer to define
' the granularity of tracing.
Public Const SEVERE = 1
Public Const WARNING = 2
Public Const IMPORTANT = 3
Public Const INFO = 4


' The log threshold sets the tracing granularity.
Public Const LogThreshold = 3



'***************************************************************
'
'            FUNCTIONS
'

' This function adds a Logger to the form. A logger is a label in a fixed location
' of the form that displays the progress of a client. It offers only one line
' and previous information in that line is always overwritten with the latest update.
Public Sub addLogger(theLogger As String)
    
    On Error GoTo newLogger
        ' a dummy assignemt to test if the logger is already there
        Dim tmpStr As String
        tmpStr = MacroOutput.ProgressFrame.Controls(theLogger).Caption
        GoTo loggerProvided

newLogger:
    Dim lbControl As Object
    Set lbControl = MacroOutput.ProgressFrame.Controls.Add("Forms.Label.1", theLogger, True)
    

    Dim theCount As Integer
    theCount = MacroOutput.ProgressFrame.Controls.Count
    
    lbControl.WordWrap = True
    lbControl.Caption = theLogger
    lbControl.Width = MacroOutput.ProgressFrame.Width
    lbControl.Height = lbControl.Height * 1.5
    MacroOutput.ProgressFrame.ScrollHeight = lbControl.Height * (theCount + 1)
        
        
    'Call MacroOutput.ProgressFrame.Scroll(fmScrollActionEnd)
    
loggerProvided:
    If theCount > 1 Then
        Call repaint
    Else
        If MacroOutput.Visible = False Then
            Call showProgressBar(False)
        End If
        MacroOutput.RevisionLabel.Caption = "Revision from: " & REVISION
        MacroOutput.Show
        MacroOutput.TraceOutput.SetFocus
        Application.ScreenUpdating = False
        Application.DisplayAlerts = False
        Debug.Print "_______________________________________________"
        Debug.Print "       >>>      START TRACING     <<<"
        Debug.Print "       at: " & Now
        Debug.Print "_______________________________________________"
    End If
End Sub

' Removes a logger from the form and repaints the existing loggers.
Public Sub removeLogger(theLogger As String)
  On Error GoTo skip
    
    MacroOutput.ProgressFrame.Controls.Remove (theLogger)
    
  On Error GoTo 0
skip:
    Call repaint
End Sub

' Recalculates the proper location of all loggers. This function is used
' by "removeLogger".
Private Sub repaint()
    Dim theCount As Integer
    Dim aControl As Object
    Dim numberOfLoggers As Integer
    
    numberOfLoggers = MacroOutput.ProgressFrame.Controls.Count
    For theCount = 0 To numberOfLoggers - 1
        Set aControl = MacroOutput.ProgressFrame.Controls(numberOfLoggers - theCount - 1)
        Call aControl.Move(5, aControl.Height * (theCount + 0.5))
    Next theCount
    MacroOutput.ProgressFrame.repaint
End Sub

' Updates the text of the Logger with the latest information. The old string
' will be overwritten.
Public Sub updateLogger(theLogger As String, theText As String)
    MacroOutput.ProgressFrame.Controls(theLogger).Caption = "<" & theLogger & "> " & theText
    MacroOutput.repaint
End Sub

' Removes all loggers from the form. Usually called when the form is closed.
Public Sub clearLoggers()
    MacroOutput.ProgressFrame.Controls.Clear
End Sub

Public Sub closeLoggingIfNoIncident()
    If MacroOutput.TraceOutput.Text = "" Then
        Call closeLogging
    End If
End Sub

Public Sub closeLogging()
    Call clearLoggers
    MacroOutput.TraceOutput.Text = ""
    Application.DisplayAlerts = True
    Application.ScreenUpdating = True
    MacroOutput.Hide
    'End
End Sub


' A simple log utility to put some traces on the output. The utility is not designed
' for vast amounts of traces, so keep the granularity of tracing low and rather
' use the "immediate window" in the Macro-Editor for more detailed tracing.
Public Sub log(theLogger As String, level As Integer, theTrace As String)
    Dim trace As String
    If level <= LogThreshold Then
        If level = SEVERE Then
            trace = "* SEVERE * "
        ElseIf level = WARNING Then
            trace = "|Warning| "
        End If
        trace = trace & "<" & theLogger & "> " & theTrace
        Debug.Print trace
        If level = SEVERE Then
            MacroOutput.TraceOutput.Text = MacroOutput.TraceOutput.Text & Chr(13) & Chr(10) & trace
        End If
    End If
End Sub

' Call this method when an unrecoverable error occurs.
Public Sub panic(PanicMsg As String)
    MacroOutput.TraceOutput.Text = MacroOutput.TraceOutput.Text & Chr(13) & Chr(10) & "***" & PanicMsg & "***"
    Debug.Print "*** PANIC *** " & PanicMsg
    PanicWindow.PTextBox.Text = MacroOutput.TraceOutput.Text
    PanicWindow.Show
End Sub

' Show a progress bar to indicate the macro progress visually
Public Sub updateProgess(percent As Integer)
    If isProgressBarShown() = True Then
        Dim p As Integer
        p = percent
        If p > 100 Then
            p = 100
        ElseIf percent < 0 Then
            p = 0
        End If
        Dim labelWidth As Integer
        MacroOutput.pLeft.Width = MacroOutput.ProgressBar.Width * p / 100
        MacroOutput.ProgressBar.repaint
    End If
End Sub

' Make progress bar visible or invisible
Public Sub showProgressBar(showIt As Boolean)
    MacroOutput.ProgressBar.Visible = showIt
End Sub

' Returns if the progress bar is visible or not
Public Function isProgressBarShown() As Boolean
    isProgressBarShown = MacroOutput.ProgressBar.Visible
End Function

'
' End of Logging
'
'************************************
