Attribute VB_Name = "Constructors"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
' This module provides constructors for the classes of this project
'

Option Explicit

' This constructor throws an error message of the anchor of the table can't be found
Public Function New_VerticalTableP(theSheet As Worksheet, tableAnchor As String) As VerticalTable
    Set New_VerticalTableP = New VerticalTable
    Call New_VerticalTableP.initP(theSheet, tableAnchor)
End Function

' This constructor does not throw error messages
Public Function New_VerticalTable(theSheet As Worksheet, tableAnchor As String) As VerticalTable
    Set New_VerticalTable = New VerticalTable
    If New_VerticalTable.init(theSheet, tableAnchor) = False Then
        Set New_VerticalTable = Nothing
    End If
End Function

Public Function New_Logger(name As String, Optional inactive As Boolean) As Logger
    Set New_Logger = New Logger
    Call New_Logger.init(name, inactive)
End Function

