VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VerticalTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
'
' Excel VBA Class: VerticalTable
'
' DESCRIPTION:
'
' The concern of this class is to provide some basic functions
' for tables which are vertically structured.
' Rules:
' - The tables consist of one header row, containing all names of the data columns.
' - Avoid blank cells in the middle of a header row.
' - The header row will be determined by a unique string provided through the constructor.
' - There can be only one such VerticalTable per sheet.
' - There must not be any unrelated data BELOW the Vertical table.
' - You may have unrelated data above the header row of the table.
'

Option Explicit

'***************************************************************
'            CONSTANTS

Public Max_First_Data_Row
Public Max_First_Data_Col
Public Max_Consecutive_Blank_Cols
Public Max_Consecutive_Blank_Rows

'***************************************************************
'           PROPERTIES

' The row containing the column names (titles) of the table.
Public headerRow As Long

' The last column of the header row
Public lastHeaderColumn As Integer

' The first row in the table which can contain data.
Public firstDataRow As Long

' The last row in the table
Public lastDataRow As Long

' The sheet where the table is located.
Public sheet As Worksheet

' The dictionary is for performance reasons: it's quicker to lookup columns from a dictionary than traversing the header row every time
Public columnDictionary As Object


'***************************************************************
'               CONSTRUCTORS

' Default constructor, allways called via new operator
Private Sub Class_Initialize()
    Max_Consecutive_Blank_Cols = 30
    Max_Consecutive_Blank_Rows = 30
    Max_First_Data_Row = 200
    Max_First_Data_Col = 200
End Sub

' This method is the second phase constructor of the class. It determines the
' header row according to the keyword <tableAnchor>
' The first data row will be assumed right after the headerRow.
' The last data row is determined and set as well as the last column of the header row.
Public Function init(theSheet As Worksheet, tableAnchor As String) As Boolean
On Error GoTo exit_false
    Set sheet = theSheet
    If tableAnchor = "" Then
        headerRow = 0
    Else
        headerRow = findRowByKeyword(tableAnchor)
    End If
 
    If headerRow = 0 Then
        init = False
    Else
        lastHeaderColumn = lastColumn(headerRow)
        firstDataRow = headerRow + 1
        Set columnDictionary = CreateObject("Scripting.Dictionary")
        Call refresh
        init = True
    End If
exit_false:
End Function

' Same as init but it throws an error message if the anchor of the table can't be found.
Public Sub initP(theSheet As Worksheet, tableAnchor As String)
    Set sheet = theSheet
    If tableAnchor = "" Then
        headerRow = 0
    Else
        headerRow = findRowByKeyword(tableAnchor)
    End If
 
    If headerRow = 0 Then
        Call Err.Raise(513, "VerticalTable::init()", "VerticalTable::init - Could not find keyword for table anchor: " & tableAnchor)
    Else
        lastHeaderColumn = lastColumn(headerRow)
        firstDataRow = headerRow + 1
        Set columnDictionary = CreateObject("Scripting.Dictionary")
        Call refresh
    End If
End Sub

'********************************************************************
'
' INTERNAL METHODS - provide means to calculate properties of the class.
'
'********************************************************************

' Returns the last column of a row
Private Function lastColumn(aRow) As Integer
  Dim theCol As Integer
  Dim consecutiveBlankCells As Integer
  With sheet
    theCol = 1
    consecutiveBlankCells = 0
    Do
        If IsEmpty(.Cells(aRow, theCol)) Then
            consecutiveBlankCells = consecutiveBlankCells + 1
        Else
            consecutiveBlankCells = 0
            lastColumn = theCol
        End If
        theCol = theCol + 1
    Loop Until consecutiveBlankCells > Max_Consecutive_Blank_Cols Or theCol = MAX_COLUMNS
  End With
End Function

' Returns the row of the last data row of the table. If the table is
' empty, 0 is returned.
Private Sub calculateLastDataRow()
    Dim lastRow As Long
    Dim consecutiveBlankRowsCount As Integer
    Dim theRow As Long
    
    lastRow = -1
    consecutiveBlankRowsCount = 0
    theRow = firstDataRow
    Do
        If isRowEmpty_(theRow) Then
            'The current row is blank
            consecutiveBlankRowsCount = consecutiveBlankRowsCount + 1
        Else
            consecutiveBlankRowsCount = 0
            lastRow = theRow
        End If
        theRow = theRow + 1
    Loop Until consecutiveBlankRowsCount > Max_Consecutive_Blank_Rows Or theRow = MAX_ROWS
    If lastRow < 0 Then
        lastDataRow = firstDataRow
    Else
        lastDataRow = lastRow
    End If
End Sub

' Returns the column of the last data column of the table. If the table is
' empty, 0 is returned.
Private Function calculateLastDataColumn() As Integer
    calculateLastDataColumn = 0
    Dim theRow As Long
    Dim lastCol As Integer
    Dim firstRow As Long
    If headerRow = 0 Then
        firstRow = 1
    Else
        firstRow = headerRow
    End If
    For theRow = firstRow To lastDataRow
        lastCol = lastColumn(theRow)
        If lastCol > calculateLastDataColumn Then
            calculateLastDataColumn = lastCol
        End If
    Next theRow
End Function

' Checks if the row <aRow> is empty. A row is assumed empty if it contains
' Max_Consecutive_Blank_Cols consecutive blank cells in a row.
Private Function isRowEmpty_(aRow) As Boolean
    Dim theCol As Integer
    Dim consecutiveBlankCells As Integer
    
    With sheet
        theCol = 1
        consecutiveBlankCells = 0
        Do
            If IsEmpty(.Cells(aRow, theCol)) Or CStr(.Cells(aRow, theCol).Value) = "" Then
                theCol = theCol + 1
                consecutiveBlankCells = consecutiveBlankCells + 1
            Else
                consecutiveBlankCells = 0
            End If
        Loop Until consecutiveBlankCells = 0 Or consecutiveBlankCells > Max_Consecutive_Blank_Cols
        If consecutiveBlankCells > 0 Then
            isRowEmpty_ = True
        Else
            isRowEmpty_ = False
        End If
    End With
End Function

'Looks into the first <Max_First_Data_Row> rows for the <keyword>.
'If not found, 0 is returned. Otherwise, the number of the found row is returned.
Private Function findRowByKeyword(keyword As String, Optional searchUntilRow As Long, Optional searchUntilColumn As Integer) As Long
    Dim varSearchUntilRow As Long
    Dim varSearchUntilColumn As Integer
    Dim theRow As Long
    Dim found As Boolean
    
    found = False
    If searchUntilRow = 0 Then
        varSearchUntilRow = Max_First_Data_Row
    Else
        varSearchUntilRow = searchUntilRow
    End If
    If searchUntilColumn = 0 Then
        varSearchUntilColumn = Max_First_Data_Col
    Else
        varSearchUntilColumn = searchUntilColumn
    End If
    
    With sheet
        theRow = 0
        Dim theColumn
        Do
            theRow = theRow + 1
            theColumn = 0
            Do
                theColumn = theColumn + 1
                If CStr(.Cells(theRow, theColumn).Value) = keyword Then
                    found = True
                End If
            Loop Until (found = True) Or (theColumn >= varSearchUntilColumn)
            
        Loop Until (found = True) Or (theRow >= varSearchUntilRow)
    
        'Check if the keyword was found
        If found = False Then
            theRow = 0
        Else
            findRowByKeyword = theRow
        End If
    End With
End Function

'Clears the Column Dictionary. This must be called whenever columns are added or removed during runtime.
Private Sub clearColumnDictionary()
    Call columnDictionary.RemoveAll
End Sub
    
' Returns the column index of <row> which equals <tag>.
' If the column is not found, "-1" is returned.
Private Function findTagInRow(tag As String, row As Long, Optional PanicOnFailure As Boolean) As Integer
    Dim cl
    findTagInRow = -1
    If row > 0 Then
        For Each cl In Range(sheet.Cells(row, 1), sheet.Cells(row, lastHeaderColumn))
            If CStr(cl.Value) = tag Then
                findTagInRow = cl.column
                Exit For
            End If
        Next cl
        If findTagInRow = -1 Then
            If PanicOnFailure = True Then
                Dim log As Logger
                Set log = New_Logger("findTagInRow")
                log.panic ("<findTagInRow>: Did not find column with tag: <" & tag & ">")
            End If
        End If
    End If
End Function

' Finds the row that contains <theTag>. The match must be exact. The entire table is searched.
Private Function findRowInTable(theTag) As Long
    Dim aRow As Long
    Dim aColumn As Integer
    Dim found As Boolean
    found = False
    findRowInTable = NO_VALUE
    aRow = headerRow
    aColumn = 1
    While aColumn <= lastHeaderColumn And found = False
        While aRow <= lastDataRow And found = False
            If getCell(aRow, aColumn).Value = theTag Then
                findRowInTable = aRow
                found = True
            End If
            aRow = aRow + 1
        Wend
        aColumn = aColumn + 1
    Wend
End Function

'*************************************************************************************
'
' BASIC METHODS - getters, setters and basic methods to access cells, rows and columns
'
'*************************************************************************************

' Returns Cells addressed by absolute coordinates x/y
Public Function getCell(row, col) As Range
    Set getCell = sheet.Cells(row, col)
End Function

' Returns a Range that contains all data rows of the table
Public Function getDataRange() As Range
    Set getDataRange = Range(sheet.Cells(firstDataRow, 1), sheet.Cells(lastDataRow, lastHeaderColumn))
End Function

' Returns a Range that contains the entire data row <theRow>
Public Function getRow(theRow As Long) As Range
    Set getRow = Range(sheet.Cells(theRow, 1), sheet.Cells(theRow, lastHeaderColumn))
End Function

' Returns a Range that contains the entire data column <theColumn>
Public Function getColumn(theColumn) As Range
    Set getColumn = Range(sheet.Cells(firstDataRow, theColumn), _
                          sheet.Cells(lastDataRow, theColumn))
End Function

    
' Return the column that matches <name> in the header of the table. This is using a dictionary to improve performance.
' This routine assumes that no columns are added or removed during runtime - else call the refresh method
' colP panics if the column isn't found
Public Function colP(name As String) As Integer
    If columnDictionary.Exists(name) Then
        colP = columnDictionary(name)
    Else
        colP = findTagInRow(name, headerRow, True)
        Call columnDictionary.Add(name, colP)
    End If
End Function
    
' Same deal as colP but doesn't panic if the <name> is not found.
Public Function col(name As String) As Integer
    If columnDictionary.Exists(name) Then
        col = columnDictionary(name)
    Else
        col = findTagInRow(name, headerRow, False)
        Call columnDictionary.Add(name, col)
    End If
End Function

' Returns true of the given row is empty
Public Function isRowEmpty(row As Long) As Boolean
    Dim cl
    For Each cl In getRow(row)
        If cl.Value <> "" Then
            isRowEmpty = False
            Exit Function
        End If
    Next cl
    isRowEmpty = True
End Function

' Returns true if the table is empty
Public Function isTableEmpty() As Boolean
    isTableEmpty = (firstDataRow = lastDataRow) And isRowEmpty(lastDataRow)
End Function

' The function searches for <searchString> in <theColumn> and returns a negative digit if not
' found or the found row otherwise.
' The negative digit specifies where the row should have been in the table.
'
' The search is done by looking for an exact match (likewise xlWhole).
'
' **** The function presumes that the column is assorted in textual alphabetical order ****
'
Public Function fastFindStringInAssortedColumn(searchString As String, theColumn As Integer) As Long
    Dim lower As Long
    Dim upper As Long
    Dim pos As Long
    Dim res As Integer
    Dim currentToken As String

    fastFindStringInAssortedColumn = 0
    lower = firstDataRow
    upper = lastDataRow
    While upper > lower
        pos = Int((upper + lower) / 2)
        currentToken = CStr(sheet.Cells(pos, theColumn).Value)
        res = AkTextComp(searchString, currentToken)
        If res = 0 Then
            lower = pos
            GoTo exit_while
        ElseIf res = 1 Then
            lower = pos + 1
        Else
            upper = pos
        End If
    Wend
exit_while:
    While searchString = CStr(sheet.Cells(lower, theColumn).Value)
        fastFindStringInAssortedColumn = lower
        lower = lower - 1
    Wend
    If fastFindStringInAssortedColumn = 0 Then
        fastFindStringInAssortedColumn = -lower
    End If
End Function

' Finds the row that matches <theValue> in <theCol> of the table.
' If no match is found, -1 is returned
Public Function findRow(theValue, theCol As Integer) As Long
    Dim theRow As Long
    Dim foundIt As Boolean
    Dim valueStr As String

    valueStr = CStr(theValue)
    
    theRow = firstDataRow
    foundIt = False
    
 On Error GoTo NOT_FOUND
    
    While Not foundIt And theRow <= lastDataRow
        If CStr(sheet.Cells(theRow, theCol).Value) = valueStr Then
            findRow = theRow
            foundIt = True
        End If
        theRow = theRow + 1
    Wend
    
    If foundIt = False Then
NOT_FOUND:
        findRow = -1
    End If
End Function

'***************************************************************
'
'               EDIT METHODS
'
' Edit methods do modify the corresponding worksheet.
'

'Makes sure that all properties of the table are recalculated
Public Sub refresh()
    Call calculateLastDataRow
    Call calculateLastDataColumn
    Call clearColumnDictionary
End Sub

' The sub asorts the entire data range of the table according to the criteria of <theColumn>, in <theOrder> (xlAscedning,
' xlDescending), and <theMatchCase> (False, True)
Public Sub assortByColumn(theColumn As Integer, theOrder, Optional theMatchCase)
    With sheet
        Range(.Cells(firstDataRow, 1), .Cells(lastDataRow, lastHeaderColumn)).Sort _
            Key1:=Range(.Cells(firstDataRow, theColumn), .Cells(firstDataRow, theColumn)), _
            Order1:=theOrder, Header:=xlNo, OrderCustom:=1, MatchCase:=theMatchCase, Orientation:=xlTopToBottom
    End With
End Sub

' Inserts a column with header <name> at the end of the table
Public Sub appendColumn(theName As String)
    lastHeaderColumn = lastHeaderColumn + 1
    getCell(headerRow, lastHeaderColumn).Value = theName
    Call refresh
End Sub

' Inserts an empty row at row <theRow>. Everything at <theRow> and below will be shifted down.
Public Sub insertEmptyRow(theRow)
    With sheet
        .Cells(theRow, 1).EntireRow.Insert Shift:=xlDown
        If theRow > lastDataRow Then
            lastDataRow = theRow
        Else
            lastDataRow = lastDataRow + 1
        End If
    End With
End Sub

' Appends an empty row at the end of the table.
Public Sub appendEmptyRow()
    If Not isRowEmpty_(lastDataRow) Then
        lastDataRow = lastDataRow + 1
    End If
End Sub

' Clears the specified data row including the formatting
Public Sub clearDataRow(theRow As Long)
    With sheet
        .Cells(theRow, 1).EntireRow.Clear
    End With
End Sub

' Clears the specified data column including the formatting
Public Sub clearDataColumn(theColumn As Integer)
    With sheet
        Range(.Cells(firstDataRow, theColumn), .Cells(lastDataRow, theColumn)).Clear
    End With
End Sub

' Deletes all rows from the firstDataRow to the last data row.
Public Sub clearData()
    If lastDataRow > 0 Then
        With sheet
            'Make sure that all data is displayed
            If .AutoFilterMode = True Then
              On Error GoTo skipshowalldata
                .AutoFilter.ShowAllData
            End If
skipshowalldata:
            'Select the entire table through a range and clear the contents
            Range(.Cells(firstDataRow, 1), .Cells(lastDataRow, 1)).EntireRow.ClearContents
            lastDataRow = firstDataRow
        End With
    End If
End Sub

' Removes any blank lines from the data area of the table
Public Sub removeBlankLinesFromData()
    With sheet
        Dim theRow As Long
        theRow = firstDataRow
        While theRow <= lastDataRow
            If isRowEmpty_(theRow) Then
                .Cells(theRow, 1).EntireRow.Delete
                lastDataRow = lastDataRow - 1
            Else
                theRow = theRow + 1
            End If
        Wend
    End With
End Sub


'
'           END OF CLASS
'
'***************************************************************************

