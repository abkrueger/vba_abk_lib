Attribute VB_Name = "StringOps"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
' This module provides method to process strings to augment the standard functions from Excel-VBA.

Option Explicit

' Extracts a string out of <str> according to the given regular expression <thePattern>.
' Multiple matches are returned in one string separated by <separator> (default: ",").
' See this for regular expressions reference: https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx
'
Public Function getStringByRegEx(strInput As String, strPattern As String, Optional separator As String = ",") As String
        Dim result

On Error Resume Next

        Set result = getRegEx(strInput, strPattern)
        
        If result Is Nothing Then
            getStringByRegEx = ""
        Else
            Dim t
            For Each t In result
                getStringByRegEx = getStringByRegEx & separator & t
            Next t
            If getStringByRegEx <> "" Then
                getStringByRegEx = Right(getStringByRegEx, Len(getStringByRegEx) - Len(separator))
            End If
        End If
End Function

' Extracts a string out of <str> according to the given regular expression <thePattern>.
' Multiple matches are returned in an array of tokens.
' See this for regular expressions reference: https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx
'
Public Function getRegEx(strInput As String, strPattern As String)
    Dim regEx
    Set regEx = CreateObject("VBScript.RegExp")
        
    If strPattern <> "" Then
        With regEx
            .Global = True
            .MultiLine = True
            .IgnoreCase = False
            .Pattern = strPattern
        End With
    
        Dim result

On Error Resume Next

        Set getRegEx = regEx.Execute(strInput)
    Else
        getRegEx = Nothing
    End If
End Function


'The function tests how many matches a regular expression <strPattern> will have for a given string <strInput>
' It returns "0" if no matches, else the number of hits
' See this for regular expressions reference: https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx
'
Public Function testRegEx(strInput As String, strPattern As String, Optional separator As String = ",") As Integer
    Dim regEx
    Set regEx = CreateObject("VBScript.RegExp")
    
    If strPattern <> "" Then
        With regEx
            .Global = True
            .MultiLine = True
            .IgnoreCase = False
            .Pattern = strPattern
        End With
    
        Dim result

On Error Resume Next

        Set result = regEx.Execute(strInput)
        testRegEx = result.Count
    
    Else
        testRegEx = 0
    End If
End Function

' Returns the first matching subgroup matching <strPattern> in <strInput>
' Note: if there are no parenthesis () in the pattern, the result will always be ""
'       if you specify multiple () in the pattern, only the first match will be return.
Function getRegExpSub(strInput As String, strPattern As String) As String
    Dim regEx
    Set regEx = CreateObject("VBScript.RegExp")
    
    If strPattern <> "" Then
        With regEx
            .Global = True
            .MultiLine = True
            .IgnoreCase = False
            .Pattern = strPattern
        End With
    
        Dim result

On Error Resume Next

        Set result = regEx.Execute(strInput)
    
        Dim t
        For Each t In result
            Dim s
            For Each s In t.SubMatches
                If s <> "" Then
                    getRegExpSub = s
                    Exit Function
                End If
            Next s
        Next t
    End If
End Function

'Returns the character at position <pos> of <theString>
Public Function charAt(theString As String, pos As Integer) As String
    If pos < 1 Or pos > Len(theString) Then
        charAt = ""
    Else
        charAt = Mid(theString, pos, 1)
    End If
End Function

'Returns the rest of the string starting at position <pos>
Public Function restString(theString As String, pos As Integer) As String
    If Len(theString) >= pos Then
        restString = Right(theString, Len(theString) - pos + 1)
    Else
        restString = ""
    End If
End Function

'Searches the comma separated list for <searchString>. Returns TRUE if contained in the list, else FALSE.
Public Function findStringInCsvList(searchString As String, target As String) As Boolean
    Dim theCount As Integer
    Dim tokens
    tokens = Split(target, STD_FIELD_DELIMITER)
    findStringInCsvList = False
    
    If UBound(tokens) >= 0 Then
        Dim token
        For Each token In tokens
            If Trim(CStr(token)) = searchString Then
                findStringInCsvList = True
                Exit For
            End If
        Next token
    End If
End Function

' Count the number of times a substring appears in string.
Public Function CountSubStr(vText As String, vChar As String, Optional IgnoreCase As Boolean) As Integer
  If IgnoreCase Then
    vText = LCase$(vText)
    vChar = LCase$(vChar)
  End If
  Dim L As Integer
  L = Len(vText)

  vText = Replace$(vText, vChar, "")
  CountSubStr = (L - Len(vText)) / Len(vChar)
End Function

