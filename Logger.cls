VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Logger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
'
' Excel VBA Class: Logger
'
' DESCRIPTION:
'
' The purpose of this class is to provide an API to the Macro programmer
' to log the progress of macros and display the progress in a user form.
' The class is using the "Logging" module for that.
'

Option Explicit

'***************************************************************
'
'           PROPERTIES
'

' The name of the logger. It is used to identify the logger.
' It must not contain white spaces or special characters
'
Public name As String

' If activated is false, all logging activities will be suppressed and the logger will be removed from the logging form.
Public activated As Boolean

' The initialization method to set the properties of the object
Public Sub init(theName As String, Optional inactive As Boolean)
    name = theName
    If Not (IsMissing(inactive)) Then
        activated = Not (inactive)
    Else
        activated = True
    End If
    
    If activated = True Then
        Call activate
    End If
End Sub

' Activates the logger
Public Sub activate()
    activated = True
    Logging.addLogger (name)
End Sub

' Removes the logger from the output window
Public Sub deactivate()
    Call Logging.removeLogger(name)
    activated = False
End Sub

' Update the display of the Logger. The new trace string will replace the previous
' one.
Public Sub update(theTrace As String)
    If activated = True Then
        Call Logging.updateLogger(name, theTrace)
    End If
End Sub

' Call this method when an unrecoverable error occurs.
Public Sub panic(PanicMsg As String)
    Logging.panic ("<" & name & "> " & PanicMsg)
End Sub

' Send a log string to the logging trace window. The string will be appended
' to the trace window panel.
Public Sub log(theLogger As String, level As Integer, theTrace As String)
    Call Logging.log(theLogger, level, theTrace)
End Sub

' For convenience: logging on info-level
Public Sub INFO(theTrace As String)
    Call log(name, Logging.INFO, theTrace)
End Sub

' For convenience: logging on warning-level
Public Sub WARNING(theTrace As String)
    Call log(name, Logging.WARNING, theTrace)
End Sub

' For convenience: logging on severe-level
Public Sub SEVERE(theTrace As String)
    Call log(name, Logging.SEVERE, theTrace)
End Sub

' For convenience: logging on important-level
Public Sub IMPORTANT(theTrace As String)
    Call log(name, Logging.IMPORTANT, theTrace)
End Sub

'
'
'           END OF CLASS
'
'***************************************************************************


