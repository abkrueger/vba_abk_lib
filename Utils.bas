Attribute VB_Name = "Utils"
'***************************************************************
'    This file is part of abk_lib.xlam
'
'    abk_lib.xlam is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    abk_lib.xlam is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'
'    Copyright 2016-2017 Andreas B. Krueger - mail@abkweb.de
'

'***************************************************************
'    Excel VBA Modules: Utils
'

Option Explicit

' Returns true if <objCollection> has an element whose index is labelled <strName>.
Function Contains(objCollection As Object, strName As String) As Boolean
    Dim o As Object
 On Error Resume Next
    Set o = objCollection(strName)
    Contains = (Err.Number = 0)
    Err.Clear
End Function

' the function calls doEvent every doEveryNtime to yield control to the OS
Public Sub yield(theCount, doEveryNtime)
    If theCount Mod doEveryNtime = 0 Then
        Dim tmp As Integer
        tmp = DoEvents
    End If
End Sub

' Convert a date to format YYYY/MM
Public Function toYYYYMM(dt As Date)
        ' Fill months column
        Dim m As String
        If Month(dt) < 10 Then
            m = "0" & Month(dt)
        Else
            m = CStr(Month(dt))
        End If
        toYYYYMM = Year(dt) & "/" & m
End Function

' Opens an Excel Workbook based on path and file name
Function openWorkbook(aPath As String, aFile As String) As Workbook
    
  On Error GoTo cannot_open_wb

    If isWorbookOpen(aFile) = True Then
        Set openWorkbook = Workbooks(aFile)
    Else
        Set openWorkbook = Workbooks.Open(aPath & aFile, False, True)
    End If
    
    Exit Function
    
cannot_open_wb:
    Dim wbLog As Logger
    Set wbLog = New_Logger("openWorkbook")
    wbLog.SEVERE ("Unable to open the workbook: " & aPath & aFile)
    Call wbLog.deactivate
    Set openWorkbook = Nothing
End Function

' Tests if a workbook is already open
Function isWorbookOpen(aFile As String) As Boolean

On Error GoTo exit_false

    Dim book
    Set book = Workbooks(aFile)
    Dim tmp
    tmp = book.Sheets.Count
    isWorbookOpen = True
    Exit Function
    
exit_false:
    isWorbookOpen = False
End Function

'Works just like the StrComp function with vbTextCompare mode, with the only exception that blank cells are  treated as
'highest Text String Values. This is to accomodate the fact that Excel assorts tables this way.
Function AkTextComp(str1 As String, str2 As String) As Integer
    If str2 = "" Then
        If str1 = "" Then
            AkTextComp = 0
        Else
            AkTextComp = -1
        End If
    ElseIf str1 = "" Then
            AkTextComp = 1
    Else
On Error GoTo STRING_COMPARE
        If CLng(str1) < CLng(str2) Then
            AkTextComp = -1
        ElseIf CLng(str1) > CLng(str2) Then
            AkTextComp = 1
        Else
            AkTextComp = 0
        End If
        Exit Function
        'TODO: What is string is a 64 bit long or a double?
STRING_COMPARE:
        AkTextComp = StrComp(str1, str2, vbTextCompare)
    End If
End Function

